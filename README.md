# RabbitMQ
A RabbitMQ database library that makes communication with RabbitMQ a bit easier.

## Getting Started
### Prerequisites
You will need docker and docker-compose to run the tests in this library.

### Installation
Just run the included composer when you first install it.
```
docker-compose run php composer install
```
## Running the tests
```
docker-compose run php vendor/bin/phpunit
```
## Deployment
To add this library to your application, install it with Composer
```
composer require davidmaes/rabbitmq

```