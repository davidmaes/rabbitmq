<?php declare(strict_types=1);

namespace DavidMaes\RabbitMQ\Consumers;

use DavidMaes\RabbitMQ\Connection;
use DavidMaes\RabbitMQ\Handlers\Handler;
use ErrorException;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

class Consumer
{
    /**
     * @var Connection
     */
    private Connection $connection;

    /**
     * @var int
     */
    private int $messageLimit;

    /**
     * @var int
     */
    private int $consumedMessages;

    /**
     * @var string
     */
    private string $exchange;

    /**
     * @var string
     */
    private string $type;

    /**
     * @var string
     */
    private string $queue;

    /**
     * @var string
     */
    private string $key;

    /**
     * @var Handler
     */
    private Handler $handler;

    /**
     * @var AMQPChannel 
     */
    private AMQPChannel $channel;

    /**
     * Consumer constructor.
     *
     * @param Connection $connection
     * @param string $exchange
     * @param string $type
     * @param string $queue
     * @param string $key
     * @param Handler $handler
     */
    public function __construct(Connection $connection, string $exchange, string $type, string $queue, string $key, Handler $handler)
    {
        $this->connection = $connection;

        $this->messageLimit = 1;
        $this->consumedMessages = 0;

        $this->exchange = $exchange;
        $this->type = $type;
        $this->queue = $queue;
        $this->key = $key;
        $this->handler = $handler;
    }

    /**
     * @param int $messageLimit
     */
    public function setMessageLimit(int $messageLimit): void
    {
        $this->messageLimit = $messageLimit;
    }

    /**
     * Checks for messages in RabbitMQ and handles them until the messageLimit is reached.
     * @throws ErrorException
     */
    public function receive()
    {
        $this->connection->connect();
        $this->channel = $this->connection->getChannel();
        $this->channel->exchange_declare($this->exchange, $this->type, false, true, false);
        $this->channel->queue_declare($this->queue, false, true, false, false);
        $this->channel->queue_bind($this->queue, $this->exchange, $this->key);
        $this->channel->basic_consume($this->queue, '', false, false, false, false, [$this, 'handleMessage']);

        while(count($this->channel->callbacks) && $this->consumedMessages < $this->messageLimit) {
            $this->channel->wait();
        }

        $this->connection->close();
    }

    /**
     * Handles the actual message from RabbitMQ.
     *
     * This is only public because it is used as a callback.
     *
     * @param AMQPMessage $message
     */
    public function handleMessage(AMQPMessage $message)
    {
        $this->channel->basic_ack($message->getDeliveryTag());
        $this->handler->handleMessage(json_decode($message->getBody()));
        $this->consumedMessages++;
    }
}