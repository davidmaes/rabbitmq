<?php declare(strict_types=1);

namespace DavidMaes\RabbitMQ\Handlers;

use stdClass;

interface Handler
{
    /**
     * @param stdClass $message
     */
    public function handleMessage(stdClass $message): void;
}
