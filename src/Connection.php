<?php declare(strict_types=1);

namespace DavidMaes\RabbitMQ;

use Exception;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use UnexpectedValueException;

class Connection
{
    /**
     * @var string
     */
    private string $host;
    /**
     * @var int
     */
    private int $port;
    /**
     * @var string
     */
    private string $user;
    /**
     * @var string
     */
    private string $password;

    /**
     * @var ?AMQPStreamConnection
     */
    private ?AMQPStreamConnection $connection;

    /**
     * Connector constructor.
     *
     * @param string $host
     * @param int $port
     * @param string $user
     * @param string $password
     */
    public function __construct(string $host, int $port, string $user, string $password)
    {
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Connect to RabbitMQ.
     */
    public function connect()
    {
        if ($this->connection) {
            throw new UnexpectedValueException('Can not connect to connection that is already connected');
        }

        $this->connection = new AMQPStreamConnection($this->host, $this->port, $this->user, $this->password);
    }

    /**
     * @return AMQPChannel
     */
    public function getChannel(): AMQPChannel
    {
        if (!$this->connection) {
            throw new UnexpectedValueException('Can not get channel from a closed connection.');
        }

        return $this->connection->channel();
    }

    /**
     * @return bool
     */
    public function isConnected(): bool
    {
        return (bool) $this->connection;
    }

    /**
     * Close the connection.
     * @throws Exception
     */
    public function close()
    {
        if (!$this->connection) {
            throw new UnexpectedValueException('Can not close connection when it is already closed.');
        }

        $this->getChannel()->close();
        $this->connection->close();
        $this->connection = null;
    }
}
