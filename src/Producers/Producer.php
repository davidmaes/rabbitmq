<?php declare(strict_types=1);

namespace DavidMaes\RabbitMQ\Producers;

use DavidMaes\RabbitMQ\Connection;
use PhpAmqpLib\Message\AMQPMessage;
use stdClass;

class Producer
{
    /**
     * @var Connection
     */
    private Connection $connection;

    /**
     * @var string
     */
    private string $exchange;

    /**
     * @var string
     */
    private string $type;

    /**
     * @var string
     */
    private string $key;

    /**
     * Producer constructor.
     *
     * @param Connection $connection
     * @param string $exchange
     * @param string $type
     * @param string $key
     */
    public function __construct(Connection $connection, string $exchange, string $type, string $key)
    {
        $this->connection = $connection;
        $this->exchange = $exchange;
        $this->type = $type;
        $this->key = $key;
    }

    /**
     * Sends a message to the exchange of this producer.
     *
     * @param stdClass $message
     */
    public function publishMessage(stdClass $message): void
    {
        $this->connection->connect();
        $channel = $this->connection->getChannel();
        $channel->exchange_declare($this->exchange, $this->type, false, true, false);
        $channel->basic_publish(
            new AMQPMessage(
                json_encode($message)
            ),
            $this->exchange,
            $this->key
        );
        $this->connection->close();
    }
}